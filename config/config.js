var path = require('path'),
    rootPath = path.normalize(__dirname + '/..'),
    smtpTransport = require('nodemailer-smtp-transport'),
    nodemailer = require('nodemailer'),
    env = process.env.NODE_ENV || 'development';
/*var transporter =  nodemailer.createTransport(smtpTransport({

                                 host: 'email-smtp.us-east-1.amazonaws.com',
                                 port: 25,
                                 auth: {
                                     user: 'AKIAI7H6RVOS62A2BO3Q',
                                     pass: 'ApeZXMySGjMJAhBlLqFlcTHN5fKysyt43z9H3Mx3jBLI'
                                 }
                             }));*/

  /*var transporter = nodemailer.createTransport("SMTP",{
  host: "smtp.mailgun.org",
  port:25,// sets automatically host, port and connection security settings
  auth: {
    user: "postmaster@sandboxf145bb68d1dd475a9bcef8aef058126c.mailgun.org",
    pass: "5ef3546c523e298116d3ceb3b5697001"
  }
});*/
var transporter = nodemailer.createTransport("SMTP",{
  host: "smtp.mailgun.org",
  port:2525,// sets automatically host, port and connection security settings
  auth: {
    user: "postmaster@zendynamix.com",
    pass: "427d607e480c16e267bce93d7b05c663"
  }
});

var config = {
  development: {
    root: rootPath,
    app: {
      name: 'VoucherApp'
    },
    port: 5000,
    /*db: 'mongodb://localhost/VoucherApp',*/
    db:'mongodb://voucherApp:voucherApp@ds015403.mlab.com:15403/voucherdb',
    notification:{
              email:{
              isEnable:true,
              smtpConfig:transporter,
              },
              message:{
              sms:true
              }
             },
          pdfFolderName:"pdf/",
          pdfBucketName:"zd-voucher-pdf"
  },

  test: {
    root: rootPath,
    app: {
      name: 'VoucherApp'
    },
    port: 5000,
    db:'mongodb://voucherApp:voucherApp@ds015403.mlab.com:15403/voucherdb',
    notification:{
          email:{
          isEnable:false
          },
          message:{
          sms:false
          }
         },
        pdfFolderName:"pdf/",
        pdfBucketName:"zd-voucher-pdf"
  },

    production: {
    root: rootPath,
    app: {
      name: 'VoucherApp'
    },
    port: 5000,
      db:'mongodb://voucherApp:voucherApp@ds015403.mlab.com:15403/voucherdb',
     notification:{
      email:{
      isEnable:true,
      smtpConfig:transporter,
      },

      message:{
      sms:true
      }
     },
      pdfFolderName:"pdf/",
      pdfBucketName:"zd-voucher-pdf"
   }
};

module.exports = config[env];
