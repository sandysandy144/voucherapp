var express = require('express');
var glob = require('glob');
/*var favicon = require('serve-favicon');*/
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var compress = require('compression');
var methodOverride = require('method-override');
var cors = require('cors');
var hbs = require('express-handlebars');
var passport	= require('passport');
module.exports = function(app, config) {
  var env = process.env.NODE_ENV || 'development';
  app.locals.ENV = env;
  app.locals.ENV_DEVELOPMENT = env == 'development';
  // set the view engine to ejs
  app.use(cors());
 app.engine('hbs',hbs({extname:'hbs',defaultLayout:'layout', layoutsDir: config.root + '/app/views/layouts'}));
console.log(config.root);
  /*app.set('views', config.root + '/app/views');*/
 /* app.set('views',path.join(_dirname,'views'));*/
  /*app.set('view engine', 'jade');*/
  app.set('view engine', 'hbs');
  /*app.use('jquery', express.static(config.root + '/node_modules/jquery/dist/'));*/
  app.use(logger('dev'));
  /*app.use(bodyParser.json());*/
  app.use(bodyParser.json({limit: "50mb"}));
  app.use(bodyParser.urlencoded({
    extended: true
  }));
  /*// get our request parameters
  app.use(bodyParser.urlencoded({ extended: false }));
  app.use(bodyParser.json());
*/
/*// log to console
  app.use(morgan('dev'));*/

// Use the passport package in our application
  app.use(passport.initialize());

  app.use(cookieParser());
  app.use(compress());
  app.use(express.static(config.root + '/public/app'));
  app.use(methodOverride());

 //app.options('*',cors());

  var controllers = glob.sync(config.root + '/app/controllers/*.js');
  controllers.forEach(function (controller) {
    require(controller)(app);
  });

  app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
  });

  if(app.get('env') === 'development'){
    app.use(function (err, req, res, next) {
      res.status(err.status || 500);
      res.render('error', {
        message: err.message,
        error: err,
        title: 'error'
      });
    });
  }

  app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    console.log(err.stack);
    res.render('error', {
      message: err.message,
      error: {},
      title: 'error'
    });
  });

};
