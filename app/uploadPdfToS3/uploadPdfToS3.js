/**
 * Created by sandeep on 9/1/2016.
 */
var express = require('express');
var mongoose = require('mongoose');
var config = require('../../config/config')
var fs = require('fs');
var knox = require('knox');
var cors = require('cors');
var bodyParser = require('body-parser');
// Load the AWS SDK for Node.js
var AWS = require('aws-sdk');
var s3CredencilasConfiguration = require('../../config/s3Credencials.js');
var accessKeyId =  s3CredencilasConfiguration.credential.key;
var secretAccessKey = s3CredencilasConfiguration.credential.secret;

// S3 Connector
var connect = function() {
  return knox.createClient(s3CredencilasConfiguration.credential);
};

var savePdfToS3 = function(data,fileName){
  AWS.config.update({
    accessKeyId: accessKeyId,
    secretAccessKey: secretAccessKey
  });
// Create a bucket and put something in it
  /*var s3bucket = new AWS.S3({params: {Bucket: 'voucherPdfBucket'}});*/
  var s3bucket = new AWS.S3({signatureVersion: 'v4'});
  /*s3bucket.createBucket(function() {});*/
  var bdata = new Buffer(data, 'binary');
    var params = {
      Key: config.pdfFolderName+fileName,
      Body: bdata,
      Bucket: config.pdfBucketName,
      contentType: 'application/pdf'
    }
    s3bucket.upload(params, function(err, data) {
      if (err) {
        console.log("Error uploading data: ", err.stacktrace);
      } else {
        console.log("Successfully uploaded data to voucherPdfBucket/key");
      }
    });

}
module.exports={
  savePdfToS3:savePdfToS3
}
