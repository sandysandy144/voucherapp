/**
 * Created by sandeep on 9/6/2016.
 */
var express = require('express'),
  router = express.Router(),
  mongoose = require('mongoose'),
  logger = require('../../config/logger.js');
  Voucher = mongoose.model('voucher');
Schema = mongoose.Schema;
Stream = require('stream');
module.exports = function (app) {
  app.use(router);
};
var morgan      = require('morgan');
var bcrypt = require("bcryptjs")
var mongoose    = require('mongoose');
var passport	= require('passport');
var config      = require('../../config/database'); // get db config file
var User        = require('../../app/models/user'); // get the mongoose model
var jwt         = require('jwt-simple');
var templateGenerator = require('../TemplateGenerator');
var path = require('path');
var fs = require('fs');
var pdf = require('html-pdf');
// pass passport for configuration
require('../../config/passport')(passport);
router.use('/api',router);

router.post('/signup', function(req, res) {
  if (!req.body.name || !req.body.password) {
    res.json({success: false, msg: 'Please pass name and password.'});
  } else {
    var newUser = new User({
      name: req.body.name,
      password: req.body.password,
      email: req.body.email
    });
    // save the user
    newUser.save(function(err) {
      if (err) {
        return res.json({success: false, msg: 'Username already exists.'});
      }
      res.json({success: true, msg: 'Successful created new user.'});
    });
  }
});
// route to authenticate a user (POST http://localhost:5000/api/authenticate)
router.post('/authenticate', function(req, res) {
  User.findOne({
    name: req.body.name
  }, function(err, user) {
    if (err) throw err;

    if (!user) {
      res.send({success: false, msg: 'Authentication failed. User not found.'});
    } else {
      // check if password matches
      user.comparePassword(req.body.password, function (err, isMatch) {
        if (isMatch && !err) {
          // if user is found and password is right create a token
          var token = jwt.encode(user, config.secret);
          // return the information including token as JSON
          res.json({success: true, token: 'JWT ' + token});
        } else {
          res.send({success: false, msg: 'Authentication failed. Wrong password.'});
        }
      });
    }
  });
});

// route to a restricted info (GET http://localhost:8080/api/memberinfo)
router.get('/memberinfo', passport.authenticate('jwt', { session: false}), function(req, res) {
  var token = getToken(req.headers);
  if (token) {
    var decoded = jwt.decode(token, config.secret);
    User.findOne({
      name: decoded.name
    }, function(err, user) {
      if (err) throw err;

      if (!user) {
        return res.status(403).send({success: false, msg: 'Authentication failed. User not found.'});
      } else {
        res.json({success: true, msg: 'Welcome in the member area ' + user.name + '!'});
      }
    });
  } else {
    return res.status(403).send({success: false, msg: 'No token provided.'});
  }
});

getToken = function (headers) {
  if (headers && headers.authorization) {
    var parted = headers.authorization.split(' ');
    if (parted.length === 2) {
      return parted[1];
    } else {
      return null;
    }
  } else {
    return null;
  }
};

