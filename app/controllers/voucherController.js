var express = require('express'),
  router = express.Router(),
  mongoose = require('mongoose'),
  logger = require('../../config/logger.js');
  Voucher = mongoose.model('voucher');
 /* companyLog = mongoose.model('logo'),*/
    /*jsreport =  require("jsreport");*/
  Schema = mongoose.Schema;
  Stream = require('stream');
  module.exports = function (app) {
  app.use(router);
 };
var morgan      = require('morgan');
var mongoose    = require('mongoose');
var passport	= require('passport');
var config      = require('../../config/database'); // get db config file
var User        = require('../../app/models/user'); // get the mongoose model
var jwt         = require('jwt-simple');
var templateGenerator = require('../TemplateGenerator');
var path = require('path');
var fs = require('fs');
var pdf = require('html-pdf');
// pass passport for configuration
require('../../config/passport')(passport);


var uploadPdfToS3 = require('../uploadPdfToS3');
// img path
var imgPath = path.join('__dirname','..','app','logo','zenDynamix.png');
console.log(imgPath);

util = require("util");

var mime = require("mime");

var dataUri = base64Image(imgPath);
/*console.log(dataUri);*/

function base64Image(src) {
  var data = fs.readFileSync(src).toString("base64");
  return util.format("data:%s;base64,%s", mime.lookup(src), data);
}
router.use('/api', router);

getToken = function (headers) {
  if (headers && headers.authorization) {
    var parted = headers.authorization.split(' ');
    if (parted.length === 2) {
      return parted[1];
    } else {
      return null;
    }
  } else {
    return null;
  }
};
router.post('/voucher',passport.authenticate('jwt', { session: false}),function(req, res) {
  var token = getToken(req.headers);
    if (token) {
      var decoded = jwt.decode(token, config.secret);
      User.findOne({name: decoded.name}, function(err, user) {
        if (err) throw err;

        if (!user) {
          return res.status(403).send({success: false, msg: 'Authentication failed. User not found.'});
        } else {
          var voucherDetails = new Voucher;
          voucherDetails.voucherId = req.body.voucherId;
          voucherDetails.name = req.body.selectName.name;
          if(req.body.chequeNo){
            voucherDetails.chequeNo = req.body.chequeNo;
          }
          voucherDetails.cash = req.body.grossAmt;
          voucherDetails.grossAmt = req.body.grossAmt;
          voucherDetails.tds = req.body.tds;
          voucherDetails.netAmt = req.body.netAmt;
          voucherDetails.approvedBY = req.body.approve;
          voucherDetails.paidBy = req.body.paid;
          voucherDetails.Date = req.body.date;
          voucherDetails.Description = req.body.desc;
          voucherDetails.companyLogo = dataUri/*req.body.companyLogo*/;
          voucherDetails.issuerSignature=req.body.issuerSignature;
          voucherDetails.recipientSignature = req.body.recipientSignature;
          voucherDetails.signature= req.body.signature;
          voucherDetails.amountInWords=req.body.totalAmtInWords;
          voucherDetails.save(function (err, resp) {
            if (err) {
              console.log(err.stacktrace);
              res.send(err);
              logger.info("cannot add voucher request");
            }
            else{
              /*  console.log(resp);*/
              res.send("saved voucher details" + " " + resp);
            }
          })
          /*res.json({success: true, msg: 'Welcome in the member area ' + user.name + '!'});*/
        }
      });
    } else {
      return res.status(403).send({success: false, msg: 'No token provided.'});
    }
  })

router.get('/voucherId',passport.authenticate('jwt', { session: false}),function(req, res) {
/*  .get(function (req, res) {*/
  var token = getToken(req.headers);
  if (token) {
    var decoded = jwt.decode(token, config.secret);
    User.findOne({
      name: decoded.name
    }, function(err, user) {
      if (err) throw err;

      if (!user) {
        return res.status(403).send({success: false, msg: 'Authentication failed. User not found.'});
      } else {
        Voucher.find().count(function (err, voucher) {
          if (err) {
            res.send(err)
          }
          else {
            console.log("get COunt")
            /*console.log(voucher)*/
            res.send({'voucherCount': voucher})
          }
        })
      /*  res.json({success: true, msg: 'Welcome in the member area ' + user.name + '!'});*/
      }
    });
  } else {
    return res.status(403).send({success: false, msg: 'No token provided.'});
  }
  })


/*router.route('/store/signature')
  .post(function (req, res) {
    console.log(req.body.signature);
    var voucherDetails = new Voucher;
    voucherDetails.signature = req.body.signature;
    voucherDetails.save(function (err, resp) {
      if (err) {
        console.log(err);
        res.send(err);
        logger.info("Error:Signature not saved");
      }
      console.log(resp);
      res.send("Signature saved" + " " + resp);
    })
  })*/


router.get('/list/voucher',passport.authenticate('jwt', { session: false}),function(req, res) {
  var token = getToken(req.headers);
  if (token) {
    var decoded = jwt.decode(token, config.secret);
    User.findOne({
      name: decoded.name
    }, function(err, user) {
      if (err) throw err;

      if (!user) {
        return res.status(403).send({success: false, msg: 'Authentication failed. User not found.'});
      } else {
        /*res.json({success: true, msg: 'Welcome in the member area ' + user.name + '!'});*/
        Voucher.find({},'voucherId name netAmt Date Description approvedBY paidBy',function(err,result){
          if(err){
            res.send(err);
          }
          else{
            console.log(result);
            res.send(result);
          }
        })
      }
    });
  } else {
    return res.status(403).send({success: false, msg: 'No token provided.'});
  }
})

    router.get('/voucher/for/template/:voucherId',passport.authenticate('jwt', { session: false}),function(req, res) {
      var token = getToken(req.headers);
      if (token) {
        var decoded = jwt.decode(token, config.secret);
        User.findOne({
          name: decoded.name
        }, function(err, user) {
          if (err) throw err;

          if (!user) {
            return res.status(403).send({success: false, msg: 'Authentication failed. User not found.'});
          } else {
           /* res.json({success: true, msg: 'Welcome in the member area ' + user.name + '!'});*/
            res.set('Content-Type','text/pdf')
            res.set('Content-Type', 'application/octet-stream');
            res.set('Content-Disposition', 'disp-ext-type; filename=voucher.pdf;creation-date-parm:'+new Date());

            Voucher.findOne({"voucherId":req.params.voucherId},function(err,obj){
              if(err){
                res.send(err);
              }
              else{
                console.log(obj);
                templateGenerator.context.createContextForVoucher(obj,function(htmlObj){
                  /*jsreport.render(htmlObj).then(function(out) {
                   console.log(out)
                   out.stream.pipe(res);
                   }).catch(function(e) {
                   res.end(e.message);
                   });*/
                  /*res.send(htmlObj);*/

                  //Used HTML to PDF converter that uses phantomjs

                    var html = htmlObj; /*fs.readFileSync(htmlObj, 'utf8');  8.27 inches x 11.69 inches.*/
                  var options = {
                    /*"height": "8in",        // allowed units: mm, cm, in, px
                     "width": "10.5in",*/
                    "format": "Tabloid",        // allowed units: A3, A4, A5, Legal, Letter, Tabloid
                    "orientation": "landscape", // portrait or landscape
                    /*"footer": {
                     "height": "28mm",
                     "contents": '<span style="color: #444;">{{page}}</span>/<span>{{pages}}</span>'
                     },*/
                    "type": "pdf",             // allowed file types: png, jpeg, pdf
                    "quality": "75", // only used for types png & jpeg
                  };
                  var d = new Date(obj.Date);
                  var date =d.toDateString();
                  console.log(date);
                  var fileName = obj.voucherId+obj.name+date+'.pdf';
                  console.log(fileName);
                  var rp = path.join('__dirname','..','app','myPDF',fileName);
                  console.log(rp);
                  pdf.create(html, options).toFile(rp, function(err, file) {
                    if (err) return console.log(err);
                    console.log(file); // { filename: '/app/myPDF/voucher.pdf' }
                    if(file){
                      fs = require('fs')
                      fs.readFile(rp, function (err,data) {
                        if (err) {
                          return console.log(err);
                        }
                        console.log(data);
                        uploadPdfToS3.uploadPdf.savePdfToS3(data,fileName);
                        res.send(data);

                      });
                    }

                  });
                  /*var page = require('webpage').create(),filename= rp;
                   page.viewportSize = {width:800,height:600};
                   page.size = {
                   format:'A4',
                   orientation:'portrait'
                   };
                   page.open(htmlObj,function(){
                   page.render(filename);
                   phantom.exit();
                   })*/
                });
              }
            })
          }
        });
      } else {
        return res.status(403).send({success: false, msg: 'No token provided.'});
      }

   /* var PDF = require('pdfkit');            //including the pdfkit module
    var fs = require('fs');
    var text = '<h1>Hello</h1>'+'ANY_TEXT_YOU_WANT_TO_WRITE_IN_PDF_DOC';

    doc = new PDF('html');//creating a new PDF object
    console.log(__dirname);
  /!*  var rp =  fs.realpathSync(__dirname+'../myPDF/test.pdf');*!/
    var rp = path.join('__dirname','..','app','myPDF','test.pdf')
    console.log(rp);
    doc.pipe(fs.createWriteStream(rp));  //creating a write stream
    //to write the content on the file system
    doc.text(text, 100, 100);             //adding the text to be written,
    // more things can be added here including new pages
    doc.end();*/
    /*var name = "suhass"
    var http = require('http');
    var jsreport = require('jsreport');
    jsreport.render("<!DOCTYPE html>"+
      "<html>"+
      "<body>"+
      "<form>"+
      "<fieldset>"+
      "<legend>Voucher Details:</legend>"+
      "name:"+name+
    "<br><br>"+
    "Total Amount Paid:{{obj.grossAmt}}"+
    "<br><br>"+
    "Date:{{obj.Date}}"+
    "<br><br>"+
    "Description:{{obj.Description}}"+
    "<br><br>"+
    "Approved By:{{obj.approvedBY}}"+
    "<br><br>"+
    "Paid By:{{obj.paidBy}}"+
    "<br><br>"+
    "</fieldset>"+
    "</form>"+
    "</body>"+
    "</html>").then(function(out) {
      console.log(out)
        out.stream.pipe(res);
      }).catch(function(e) {
        res.end(e.message);
      });
*/
    /*var jr =require("jsreport")
    jr.render({
      template: {
        content: "Hello world from {{#sayLoudly this.name}}",
        helpers: "function sayLoudly(str) { return str.toUpperCase(); }",
        engine: "handlebars"
      },
      data: { name: "jsreport" }
    }).then(function(out) {
      //pipes pdf with Hello world from JSREPORT
      out.stream.pipe(res);
    });*/

   /* require("jsreport").render({
      template: {
        content: "<h1>Hello world from "+name+"</h1>",
        recipe: "html"
      },
      data: { name: "jsreport" }
    }).then(function(out) {
      console.log("_____________");
      //pipes plain text with Hello world from jsreport
      out.stream.pipe(res);
    });*/

  })

/*router.route('/voucher/for/id/:id')
  .get(function(req,res){
  /!*  res.set('Content-Type','text/pdf')
    res.set('Content-Type', 'application/octet-stream');
    res.set('Content-Disposition', 'disp-ext-type; filename=voucher.pdf;creation-date-parm:'+new Date());*!/
    Voucher.findOne({"voucherId":req.params.id},function(err,obj){
      if(err){
        res.send(err);
      }
      else{
        console.log(obj);
          res.send(obj);
      }
    })
  })*/


/*router.route('/voucher/:id')
  .get(function(req,res){
   /!* res.set('Content-Type','text/pdf')
    res.set('Content-Type', 'application/octet-stream');
    res.set('Content-Disposition', 'disp-ext-type; filename=voucher.pdf;creation-date-parm:'+new Date());*!/
    Voucher.findOne({"voucherId":req.params.id},function(err,obj){
      if(err){
        res.send(err);
      }
      else{
        console.log(obj);
        res.send(obj)
      }
    })
  })*/

router.get('/voucher/skip/limit/:skip/:limit',passport.authenticate('jwt', { session: false}),function(req, res) {
  var token = getToken(req.headers);
  if (token) {
    var decoded = jwt.decode(token, config.secret);
    User.findOne({
      name: decoded.name
    }, function(err, user) {
      if (err) throw err;

      if (!user) {
        return res.status(403).send({success: false, msg: 'Authentication failed. User not found.'});
      } else {
       /* res.json({success: true, msg: 'Welcome in the member area ' + user.name + '!'});*/
        var skip = parseInt(req.params.skip);
        var limit = parseInt(req.params.limit);
        Voucher.find().skip(skip).limit(limit).sort({'createdAt': -1})
          .exec(function (err, requests) {
            if (err){
              console.log(err.stack);
              return err;
            }
            console.log('The creator is %s', requests);
            res.send(requests);
          });
      }
    });
  } else {
    return res.status(403).send({success: false, msg: 'No token provided.'});
  }
})

router.get('/voucher/aggregation/:searchName',passport.authenticate('jwt', { session: false}),function(req, res) {
  var token = getToken(req.headers);
  if (token) {
    var decoded = jwt.decode(token, config.secret);
    User.findOne({
      name: decoded.name
    }, function(err, user) {
      if (err) throw err;

      if (!user) {
        return res.status(403).send({success: false, msg: 'Authentication failed. User not found.'});
      } else {
       /* res.json({success: true, msg: 'Welcome in the member area ' + user.name + '!'});*/
        console.log(req.params.searchName);
        Voucher.aggregate(
          // Pipeline
          [
            // Stage 1
            {
              $match: {
                "name":{$regex:req.params.searchName, $options: '"$i"'}
              }
            },

            // Stage 2
            {
              $limit: 5
            },

            // Stage 3
            {
              $group: {
                "_id":"$name"
              }
            },

            // Stage 4
            {
              $project: {
                "_id":0,
                "value":"$_id"
              }
            }

          ],function(err,name){
            if(err)
              res.send(err);
            res.send(name);
          });

        // Created with 3T MongoChef, the GUI for MongoDB - http://3t.io/mongochef
      }
    });
  } else {
    return res.status(403).send({success: false, msg: 'No token provided.'});
  }
})

router.get('/voucher/details/name/:name',passport.authenticate('jwt', { session: false}),function(req, res) {
  var token = getToken(req.headers);
  if (token) {
    var decoded = jwt.decode(token, config.secret);
    User.findOne({
      name: decoded.name
    }, function(err, user) {
      if (err) throw err;

      if (!user) {
        return res.status(403).send({success: false, msg: 'Authentication failed. User not found.'});
      } else {
       /* res.json({success: true, msg: 'Welcome in the member area ' + user.name + '!'});*/
        Voucher.findOne({"name":req.params.name},function(err,result){
          if(err){
            console.log(err.stacktrace)
          }
          else{
            console.log(result);
            res.send(result)
          }
        })
      }
    });
  } else {
    return res.status(403).send({success: false, msg: 'No token provided.'});
  }
})
