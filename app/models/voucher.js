var mongoose = require('mongoose'),
 Schema = mongoose.Schema;
var voucherSchema = new mongoose.Schema(
  {
    "voucherId": String,
    "name": String,
    "amountInWords": String,
    "grossAmt": Number,
    "tds": Number,
    "netAmt": Number,
    "Date": Date,
    "chequeNo":String,
    "cash":Number,
    "approvedBY": String,
    "paidBy": String,
    "companyLogo":String,
    "issuerSignature":String,
    "recipientSignature": String,
    "signature": String,
    "Description": String,
    "img": { data: Buffer, contentType: String },
    createdAt: {type: Date, default: Date.now},
    updateAt: {type: Date, default: Date.now},
  }, {collection: "voucherDetails"});

voucherSchema.pre('update', function () {
  this.update({}, {$set: {updateAt: new Date()}});
  // next();
})

/*+++++++++++++++++++++++++++++++++++++++++  Defining Model  Starts++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
mongoose.model('voucher', voucherSchema);
