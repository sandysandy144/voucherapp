'use strict';

var request = require('request');

module.exports = function (grunt) {
  // show elapsed time at the end
  require('time-grunt')(grunt);
  // load all grunt tasks
  require('load-grunt-tasks')(grunt);

  var reloadPort = 35737, files;

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    develop: {
      server: {
        file: 'app.js'
      }
    },
    watch: {
          options: {
            nospawn: true,
            livereload: reloadPort
          },
          js: {
            files: [
              'app.js',
              'app/**/*.js',
              'config/*.js'
            ],
            tasks: ['develop', 'delayed-livereload']
          },
          css: {
            files: [
              'public/css/*.css'
            ],
            options: {
              livereload: reloadPort
            }
          },
          views: {
            files: [
              'app/views/*.jade',
              'app/views/**.jade'
            ],
            options: { livereload: reloadPort }
          }
          },
    uglify: {
              options: {
                mangle: false
              },
              my_target: {
                files: {
                  '../relocationwebappdistribution/public/app/libraries/minifiedscripts/appScript-min.js':['public/app/libs/angular/angular.min.js','public/app/libs/jquery/dist/jquery.min.js',
                  'public/app/libs/check-box/checklist-model.js','public/app/libs/angular-route/angular-route.js','public/app/libs/bootstrap/dist/js/bootstrap.min.js',
                  'public/app/libs/angular-animate/angular-animate.min.js','public/app/scripts/myDirectives/htmlBindCompileDirective.js','public/app/scripts/myDirectives/calendarDirective.js',
                  'public/app/scripts/myDirectives/ngChangeOnBlur.js','public/app/scripts/myDirectives/updateHiddenField.js',
                  'public/app/scripts/controller/controllers.js','public/app/scripts/controller/customerCtrl.js','public/app/scripts/controller/relocationRequestCtrl.js','public/app/scripts/controller/otpEnable.js',
                  'public/app/scripts/controller/customerTypeHeadCtrl.js','public/app/scripts/controller/datepickerCtrl.js','public/app/scripts/controller/scrollCtrl.js',
                  'public/app/scripts/myDirectives/jqueryCheckBox.js','public/app/scripts/route/relocationRoute.js','public/app/scripts/service/reliocationService.js','public/app/scripts/service/AuthenticationService.js','public/app/scripts/controller/LoginController.js','public/app/scripts/myDirectives/openAccordion.js','public/app/libs/angular-bootstrap/ui-bootstrap-tpls.min.js']
                                               }
              }
            },
    processhtml: {
           build: {
             files: {
               '../relocationwebappdistribution/public/app/index.html': ['public/app/index.html']
             }
           }
         },
    copy: {
                  main: {
                    files: [
                       {expand: true, cwd: 'app/', src: ['**'], dest: '../relocationwebappdistribution/app/'},
                       {expand: true, cwd: 'config/', src: ['**'], dest: '../relocationwebappdistribution/config/'},
    /*                 {expand: true, cwd: 'public/app', src: ['**'], dest: '../relocationwebappdistribution/public/app'},*/
                       {expand: true, cwd: 'public/', src: ['**','!**/scripts/**','!**/styles/**','!**/libs/**','!index.html'], dest: '../relocationwebappdistribution/public/'},
                       { src: "package.json", dest: '../relocationwebappdistribution/'},
                       { src:"app.js", dest: '../relocationwebappdistribution/'},
                       { src:"pm2.json", dest: '../relocationwebappdistribution/'},
                       {expand: true, cwd: 'public/app/libs/bootstrap', src: ['**/fonts/*'], dest: '../relocationwebappdistribution/public/app/libaries/'},
                      ]
                   }
                 },
    cssmin: {

          target: {
            files: {
              '../relocationwebappdistribution/public/app/libaries/cssMinified/template-min.css': ['public/app/libs/bootstrap/dist/css/bootstrap.min.css','public/app/styles/AdminLTE.css',
              'public/app/styles/animation.css','public/app/libs/angular-bootstrap/ui-bootstrap-csp.css','public/app/styles/relocationAppStyles.css']
            }
          }
        },
    hashres: {
               options: {
                 encoding:'utf8',
                 fileNameFormat: '${name}.${hash}.cache.${ext}',
                 renameFiles: true
               },
               prod: {
                 options: {
                 },
                  src: ['../relocationwebappdistribution/public/app/libaries/cssMinified/template-min.css','../relocationwebappdistribution/public/app/minifiedscripts/appminScript-min.js'],
                  dest: ['../relocationwebappdistribution/public/**/*.html']
               }
             }

  });

  grunt.config.requires('watch.js.files');
  files = grunt.config('watch.js.files');
  files = grunt.file.expand(files);
  // load required npm plugins
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-processhtml');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
     grunt.loadNpmTasks('grunt-hashres');

  grunt.registerTask('delayed-livereload', 'Live reload after the node server has restarted.', function () {
    var done = this.async();
    setTimeout(function () {
      request.get('http://localhost:' + reloadPort + '/changed?files=' + files.join(','),  function(err, res) {
          var reloaded = !err && res.statusCode === 200;
          if (reloaded)
            grunt.log.ok('Delayed live reload successful.');
          else
            grunt.log.error('Unable to make a delayed live reload.');
          done(reloaded);
        });
    }, 500);
  });

  grunt.registerTask('default', [
    'develop',
    'watch'

  ]);

    grunt.registerTask('production', [
            'uglify','copy','processhtml','cssmin','hashres'
    ]);
};
